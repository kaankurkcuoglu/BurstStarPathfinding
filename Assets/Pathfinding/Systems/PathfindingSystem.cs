﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace BattleSimulator.Pathfinding
{
	public class PathfindingSystem : MonoBehaviour
	{
		public NativeArray<PathNode> PathNodes;
		public int2 GridSize;


		private void OnDisable()
		{
			if(PathNodes.IsCreated)
				PathNodes.Dispose();
		}

		private void Update()
		{
			if(!PathNodes.IsCreated)
				return;

			var gridSize = GridSize;
			var pathNodes = PathNodes;
			var nodeCount = pathNodes.Length;
			var copyPathNodes = new NativeArray<PathNode>(nodeCount, Allocator.TempJob,
				NativeArrayOptions.UninitializedMemory);


			for (int iNode = 0; iNode < pathNodes.Length; iNode++)
			{
				copyPathNodes[iNode + nodeCount] = pathNodes[iNode];
			}



		}

		
	}
}