﻿using Unity.Mathematics;

namespace BattleSimulator.Pathfinding
{
	public struct PathNode
	{
		public int2 Position;
		public bool IsWalkable;
		public int Index;
		public int CameFromNodeIndex;
		public int CostFromStartNode;
		public int Heuristic;
		public int Priority => CostFromStartNode + Heuristic;
	}
}