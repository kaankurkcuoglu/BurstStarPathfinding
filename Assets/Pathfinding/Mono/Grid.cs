﻿using System.Collections;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

namespace BattleSimulator.Pathfinding
{
	public class Grid : MonoBehaviour
	{
		public MeshRenderer MeshRenderer;
		public TextMeshProUGUI CoordsText;
		public int2 Coords;

		bool IsChangingColor;

		public void Initialize(int2 coords, float scale, bool isWalkable)
		{
			SetCoords(coords);
			transform.localScale = Vector3.one * scale;
			transform.position = new Vector3(coords.x * scale, 0f, coords.y * scale);

			if(!isWalkable)
				SetColor(Color.black);
		}

		void SetCoords(int2 coords)
		{
			Coords = coords;
		}

		public void SetColor(Color color)
		{
			MeshRenderer.material.color = color;
		}

		public void SetColorForDuration(Color color, float duration)
		{
			StartCoroutine(_SetColorForDuration(color, duration));
		}

		IEnumerator _SetColorForDuration(Color color, float duration)
		{
			if(IsChangingColor)
				yield break;

			IsChangingColor = true;
			var originalColor = MeshRenderer.material.color;
			SetColor(color);
			yield return new WaitForSeconds(duration);
			SetColor(originalColor);
			IsChangingColor = false;
		}
	}
}