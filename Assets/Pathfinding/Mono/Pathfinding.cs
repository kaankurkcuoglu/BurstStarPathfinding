using BattleSimulator.Pathfinding;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace Pathfinding.Mono
{
	[BurstCompile]
	public struct FindPathJob : IJob
	{
		public NativeArray<PathNode> PathNodes;
		public int2 GridSize;
		public int2 StartPosition;
		public int2 EndPosition;
		public NativeList<int2> Path;
		public NativeArray<bool> Result; // No other way to return bool inside a job, use nativearray with size 1.

		public void Execute()
		{
			Result[0] = Pathfinding.FindPath(StartPosition, EndPosition, GridSize, PathNodes, Path);
		}
	}

	public static class Pathfinding
	{
		public static bool FindPath(
			int2 startPosition, int2 endPosition, int2 gridSize,
			NativeArray<PathNode> pathNodeArray, NativeList<int2> resultPath)
		{
			for(int i = 0; i < pathNodeArray.Length; i++)
			{
				var pathNode = pathNodeArray[i];
				pathNode.Heuristic = CalculateDistanceCost(pathNode.Position, endPosition);
				pathNode.CostFromStartNode = int.MaxValue;
				pathNode.CameFromNodeIndex = -1;
				pathNodeArray[i] = pathNode;
			}

			var endNodeIndex = CalculateNodeIndex(endPosition, gridSize.x);

			// Initialize Neighbor Offset Array
			var neighborOffsetArray = new NativeArray<int2>(8, Allocator.Temp, NativeArrayOptions.UninitializedMemory);

			neighborOffsetArray[0] = new int2(-1, 0); // Left 
			neighborOffsetArray[1] = new int2(1, 0); // Right
			neighborOffsetArray[2] = new int2(0, 1); // Up
			neighborOffsetArray[3] = new int2(0, -1); // Down
			neighborOffsetArray[4] = new int2(-1, -1); // Left Down  
			neighborOffsetArray[5] = new int2(-1, 1); // Left Up 
			neighborOffsetArray[6] = new int2(1, -1); // Right Down
			neighborOffsetArray[7] = new int2(1, 1); // Right Up 

			// Initialize StartNode Cost to 0
			var startIndex = CalculateNodeIndex(startPosition, gridSize.x);
			var startNode = pathNodeArray[startIndex];
			startNode.CostFromStartNode = 0;
			pathNodeArray[startNode.Index] = startNode;

			var frontierNodeIndexes = new NativeList<int>(Allocator.Temp);
			var searchedNodeIndexes = new NativeList<int>(Allocator.Temp);

			frontierNodeIndexes.Add(startNode.Index);

			while(frontierNodeIndexes.Length > 0)
			{
				var currentNodeIndex = GetLowestPriorityNodeIndex(frontierNodeIndexes, pathNodeArray);
				var currentNode = pathNodeArray[currentNodeIndex];

				if(currentNodeIndex == endNodeIndex)
				{
					// Reached destination
					break;
				}

				// Remove current node from the Open List
				for(int i = 0; i < frontierNodeIndexes.Length; i++)
				{
					if(frontierNodeIndexes[i] == currentNodeIndex)
					{
						frontierNodeIndexes.RemoveAtSwapBack(i);
						break;
					}
				}

				// Add current node to the closed list
				searchedNodeIndexes.Add(currentNodeIndex);

				for(int i = 0; i < neighborOffsetArray.Length; i++)
				{
					var neighborOffset = neighborOffsetArray[i];
					var neighborPosition = currentNode.Position + neighborOffset;

					if(!IsPositionInsideGrid(neighborPosition, gridSize))
					{
						// Neighbor does not exist for position
						continue;
					}

					var neighborIndex = CalculateNodeIndex(neighborPosition, gridSize.x);

					if(searchedNodeIndexes.Contains(neighborIndex))
					{
						// Already searched this node
						continue;
					}

					var neighborNode = pathNodeArray[neighborIndex];

					if(!neighborNode.IsWalkable)
					{
						// Not Walkable
						continue;
					}

					var tentativeCost = currentNode.CostFromStartNode +
						CalculateDistanceCost(currentNode.Position, neighborNode.Position);

					// Only update neighbor node if the found cost is lower
					if(tentativeCost < neighborNode.CostFromStartNode)
					{
						neighborNode.CameFromNodeIndex = currentNodeIndex;
						neighborNode.CostFromStartNode = tentativeCost;
						pathNodeArray[neighborIndex] = neighborNode;

						if(!frontierNodeIndexes.Contains(neighborIndex))
						{
							frontierNodeIndexes.Add(neighborIndex);
						}
					}
				}
			}

			var endNode = pathNodeArray[endNodeIndex];

			// Clear the DynamicBuffer before adding to it
			resultPath.Clear();

			var isPathFound = CalculatePath(pathNodeArray, endNode, resultPath);

			frontierNodeIndexes.Dispose();
			searchedNodeIndexes.Dispose();
			neighborOffsetArray.Dispose();

			return isPathFound;
		}

		static bool CalculatePath(NativeArray<PathNode> pathNodeArray, PathNode endNode, NativeList<int2> resultPath)
		{
			if(endNode.CameFromNodeIndex == -1)
			{
				// Couldn't find a path
				return false;
			}

			// Found a path
			resultPath.Add(endNode.Position);

			var currentNode = endNode;

			while(currentNode.CameFromNodeIndex != -1)
			{
				var cameFromNode = pathNodeArray[currentNode.CameFromNodeIndex];
				resultPath.Add(cameFromNode.Position);
				currentNode = cameFromNode;
			}

			return true;
		}

		public static bool IsPositionInsideGrid(int2 position, int2 gridSize)
		{
			return
				position.x >= 0 &&
				position.y >= 0 &&
				position.x < gridSize.x &&
				position.y < gridSize.y;
		}

		static int GetLowestPriorityNodeIndex(NativeList<int> openList, NativeArray<PathNode> pathNodeArray)
		{
			var lowestCostPathNode = pathNodeArray[openList[0]];

			for(int i = 1; i < openList.Length; i++)
			{
				var testPathNode = pathNodeArray[openList[i]];

				if(testPathNode.Priority < lowestCostPathNode.Priority)
				{
					lowestCostPathNode = testPathNode;
				}
			}

			return lowestCostPathNode.Index;
		}

		static int CalculateDistanceCost(int2 pos1, int2 pos2)
		{
			const int MoveStraightCost = 10;
			const int MoveDiagonalCost = 14;

			var xDistance = math.abs(pos1.x - pos2.x);
			var yDistance = math.abs(pos1.y - pos2.y);
			var remaining = math.abs(xDistance - yDistance);

			return
				MoveDiagonalCost * math.min(xDistance, yDistance) +
				MoveStraightCost * remaining;
		}

		public static int CalculateNodeIndex(int2 position, int gridWidth)
		{
			return position.x + position.y * gridWidth;
		}
	}
}