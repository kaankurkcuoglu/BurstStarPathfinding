using System;
using BattleSimulator.Pathfinding;
using Pathfinding.Mono;
using Sirenix.OdinInspector;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Color = UnityEngine.Color;


public class Pathfinder : MonoBehaviour
{

	[SerializeField]
	private int2 Size;
	
	
	[SerializeField]
	private int2 StartPos;	
	
	
	[SerializeField]
	private int2 EndPos;

	private int2[] lastPath = new int2[0];
	
	
	



	[Button(ButtonSizes.Gigantic)]
	private void FindPath()
	{

		var pathNodes = new NativeArray<PathNode>(Size.x*Size.y,Allocator.TempJob);
		var path = new NativeList<int2>(0,Allocator.TempJob);
		var result = new NativeArray<bool>(1, Allocator.TempJob);

		for (var index = 0; index < pathNodes.Length; index++)
		{
			var position = new int2(index % Size.x, index / Size.y);
			pathNodes[index] = new PathNode()
			{
				Position = position,
				IsWalkable = true,
				Index = index,
				CameFromNodeIndex = -1,
				CostFromStartNode = int.MaxValue,
				Heuristic = 0,
			};
		}

		FindPathJob pathJob = new FindPathJob()
		{
			PathNodes = pathNodes,
			GridSize = Size,
			StartPosition = StartPos,
			EndPosition = EndPos,
			Path = path,
			Result = result
		};


		var jobHandle = pathJob.Schedule();

		jobHandle.Complete();

		lastPath = path.ToArray();
		
		result.Dispose();
		path.Dispose();
		pathNodes.Dispose();

	}
	private void OnDrawGizmos()
	{
		// for (int x = 0; x < Size.x; x++)
		// for (int y = 0; y < Size.y; y++)
		// {
		// 	Gizmos.color = Color.green;
		// 	Gizmos.DrawWireCube(new Vector3(x,0,y),new Vector3(1,0.01f,1));
		// }

		Gizmos.color = Color.white;
		Gizmos.DrawSphere(new Vector3(StartPos.x,0,StartPos.y),0.1f);
		Gizmos.color = Color.black;
		Gizmos.DrawSphere(new Vector3(EndPos.x,0,EndPos.y),0.1f);

		Gizmos.color = Color.red;
		for (int i = 0; i < lastPath.Length - 1; i++)
		{
			var currentPos = new Vector3(lastPath[i].x, 0, lastPath[i].y);
			var nextPos = new Vector3(lastPath[i+1].x, 0, lastPath[i+1].y);
			Gizmos.DrawLine(currentPos,nextPos);
		}
		
	}
}




